# -*- coding: utf-8 -*-
"""
Archivo de configuracion para Celery
"""
#from kombu import Queue
from datetime import timedelta

# RabbitMQ como broker para Celery
BROKER_URL = 'amqp://localhost:5672/abm'

# Results también en Rabbit
#CELERY_RESULT_BACKEND = 'redis://localhost/'
CELERY_IGNORE_RESULT = True

# Archivo donde se definen las tareas
CELERY_IMPORTS = ("webapp.tasks", )

"""
CELERY_DEFAULT_QUEUE = 'default'
CELERY_QUEUES = (
    Queue('recovery', routing_key='recovery.#'),
    Queue('low',      routing_key='low.#'),
    Queue('default',  routing_key='task.#'),
    Queue('high',     routing_key='high.#'),
)
CELERY_DEFAULT_EXCHANGE = 'tasks'
CELERY_DEFAULT_EXCHANGE_TYPE = 'topic'
CELERY_DEFAULT_ROUTING_KEY = 'task.default'

CELERY_ROUTES = {
    'backend.tasks.execute': {
        'queue':       'high',
        'routing_key': 'high.execute',
    },
    'backend.twsearch.update_captures': {
        'queue':       'high',
        'routing_key': 'high.update_captures',
    },
    'backend.notification.email_notification': {
        'queue':       'high',
        'routing_key': 'high.send_email',
    },
    'backend.twtasks.recover_account': {
        'queue':       'recovery',
        'routing_key': 'recovery.recover_account',
    }
}
"""

# Limites
CELERY_DISABLE_RATE_LIMITS = True

# Tareas programadas
CELERY_TIMEZONE = "America/Argentina/Buenos_Aires"
CELERYBEAT_SCHEDULE = {
    #'update-filters': {
    #    'task': 'backend.tasks.calculate_filters_values',
    #    'schedule': timedelta(seconds=30)
    #}
    #'update-captures': {
    #    'task': 'backend.twsearch.update_captures',
    #    'schedule': timedelta(seconds=1200)
    #}
#    'read-email-verifications': {
#        'task': 'backend.notifications.read_email_verifications',
#        'schedule': timedelta(seconds=300)
#    }
}

CELERY_ACCEPT_CONTENT = ['pickle', 'json']
CELERY_TASK_SERIALIZER = 'pickle'
#CELERY_RESULT_SERIALIZER = 'json'
