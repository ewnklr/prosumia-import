# -*- coding: utf-8 -*-
from flask.ext.assets import Environment, Bundle


#: application css bundle
css_custom = Bundle(
        "css/custom.css",
        filters="cssmin", output="css/custom.min.css")

#: consolidated css bundle
css_all = Bundle(
        "css/bootstrap-table.min.css",
        css_custom,
        filters="cssmin", output="css/abm.min.css")

#: vendor js bundle
js_vendor = Bundle(
        "js/vendor/bootstrap-table.min.js",
        "js/vendor/bootstrap-table-es-AR.min.js",
        "js/vendor/file-navigator.min.js",
        "js/vendor/line-navigator.min.js",
        filters="jsmin", output="js/vendor.min.js")

js_uploader = Bundle(
        "js/vendor/jquery.ui.widget.js",
        "js/vendor/jquery.iframe-transport.js",
        "js/vendor/jquery.fileupload.js",
        filters="jsmin", output="js/jquery-uploader.min.js")


def init_app(app):
    webassets = Environment(app)
    webassets.register('css_all', css_all)
    webassets.register('js_vendor', js_vendor)
    webassets.register('js_uploader', js_uploader)
    webassets.manifest = 'cache' if not app.debug else False
    webassets.cache = not app.debug
    webassets.debug = app.debug
