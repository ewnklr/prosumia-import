# -*- coding:utf-8 -*-
import csv, codecs, cStringIO
from functools import wraps
from flask import request, Response

class UTF8Recoder:
    """
    Iterator that reads an encoded stream and reencodes the input to UTF-8
    """
    def listreader(self, encoding, lines):
        for line in lines:
            yield line.decode(encoding)

    def __init__(self, f, encoding):
        if isinstance(f, file):
            self.reader = codecs.getreader(encoding)(f)
        else:
            self.reader = self.listreader(encoding, f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")

class UnicodeReader:
    """
    A CSV reader which will iterate over lines in the CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.DictReader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        result = {}
        for fieldname in row:
            if row[fieldname] is not None:
                result[fieldname] = unicode(row[fieldname], "utf-8")
            else:
                result[fieldname] = None
            #Lo paso a DicTeader, sino era así:
            #return [unicode(s, "utf-8") for s in row]
        return result

    @property
    def fieldnames(self):
        r= [unicode(s, "utf-8") for s in self.reader.fieldnames ]
        # FIXME no los muestra bien?
        return r

    def __iter__(self):
        return self

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


def labeldict(labels):
    """ 
    Crea un diccionario para la la lista de tuplas
    """
    result = dict()
    for name, label in labels:
        result[name] = label
    return result


def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == 'matias' and password == 'm47145!'

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

