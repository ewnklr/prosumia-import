# -*- coding:utf-8 -*-
from flask.ext.classy import FlaskView, route
from flask import redirect, url_for, session
from flask import render_template, request, jsonify, current_app
from werkzeug import secure_filename
from peewee import IntegrityError, UpdateQuery
import json
import os
import uuid
import csv
import cchardet
import codecs

from webapp.api import api
from webapp.model import DataImport, CSVMap, field_labels, status_labels
from webapp import database
from webapp import tasks
from webapp import helpers
from webapp import application


status_label_dict = helpers.labeldict(status_labels)

def detect_separator(row):
    """
    Intenta obtener los caracteres que separan campos
    """
    if row.count(",") > row.count(";"):
        separator = ","
    else:
        separator = ";"
    
    return separator


def detect_wrapper(raw_field):
    """
    Solo funciona sobre el header, esperando que no tenga el separador en el nombre
    """
    if raw_field[0] == raw_field[-1]:
        if len(raw_field) > 2:
            return raw_field[0]

    return None


def save_file(data_file, filename, encoding):
    """
    Guarda un archivo data_file codificado en encoding (windows-1252, ascii, etc) 
    en utf-8.
    """
    path = os.path.join(application.config['UPLOAD_FOLDER'], filename)
    output_file = codecs.open(path, "wb", "utf-8")
    unicode_data_file = codecs.getreader(encoding)(data_file, errors='ignore')
    while True:
        data = unicode_data_file.read(1024)
        if not data:
            break

        # Guardar el chunk
        output_file.write(data)

    output_file.close()
    file_size = os.stat(path).st_size
    return path, file_size


def get_field_value(raw, wrapper):
    if wrapper:
        return raw[1:-1]
    return raw


def create_base_import(upload_id, path, size, base_name, base_desc):
    """
    Crea la importación de una base, estas son las que consituyen la base del ABM entero
    """
    try:
        with database.transaction():
            dataimport = DataImport.create(
                    upload_id=upload_id,
                    file_path=path,
                    file_size=size,
                    name=base_name,
                    desc=base_desc,
                    status="waiting-map"
            )
    except IntegrityError as e:
        print u"Error al crear importación (%s)" % e
        return None

    return dataimport


def mapped_fields(csvmap):
    csvmap = csvmap.get()
    result = set()
    for field, label in field_labels:
        if getattr(csvmap, field):
            result.add(label)

    return list(result)


def detect_fields(data):
    first_lines = data[:data.rfind("\n")+1]
    # Guess de delimitadores y campos
    try:
        dialect = csv.Sniffer().sniff(first_lines)
        has_header = csv.Sniffer().has_header(first_lines)
    except csv.Error:
        raise ValueError

    # Detección de codificación
    guess = cchardet.detect(first_lines)

    if not has_header:
        csvreader = helpers.UnicodeReader(first_lines.splitlines(), dialect=dialect, encoding=guess['encoding'])
        fields = [ "col_%s" % (idx) for idx in range(len(csvreader.fieldnames)) ]
        #return jsonify({"error" : "El archivo no tiene cabecera"})
    else:
        csvreader = helpers.UnicodeReader(first_lines.splitlines(), dialect=dialect, encoding=guess['encoding'])
        fields = csvreader.fieldnames

    return fields


class ImportsView(FlaskView):

    def get(self):
        results = []
        query = DataImport.select(DataImport, CSVMap).join(CSVMap).order_by(DataImport.id.desc())
        for dataimport in query:
            results.append({
                "name":    dataimport.name,
                "desc":    dataimport.desc,
                "count":   dataimport.row_count,
                "fields":  ", ".join(mapped_fields(dataimport.map)),
                "status":  status_label_dict[dataimport.status],
                "actions": render_template("_actions.html")
            })

        return jsonify(
            {"data": results}
        )


    @route("/edit-upload", methods=["POST"])
    def edit_upload(self):
        upload_id = request.form.get('uploadId')
        base_name = request.form.get('baseName')
        base_desc = request.form.get('baseDesc')

        q = DataImport.select().where(DataImport.upload_id==upload_id).get()
        q.name = base_name
        q.desc = base_desc
        q.save()

        try:
            data = open(q.file_path, 'rb').read(2 * 2 ** 10)
            fields = detect_fields(data)
        except ValueError:
            return jsonify({"error" : "No se encontraron los delimitadores"})

        return jsonify({"fields" : fields, "id" : upload_id})



    @route("/upload", methods=["POST"])
    def upload(self):
        upload_id = request.form.get('uploadId')
        base_name = request.form.get('baseName')
        base_desc = request.form.get('baseDesc')
        data_file = request.files.get('inputFile')

        filename = secure_filename(upload_id + "_" + data_file.filename)

        # Muestra del archivo para detectar encoding
        sample = data_file.stream.read(10 * 2**20)
        data_file.stream.seek(0)
        guess = cchardet.detect(sample)

        # Se guarda en utf-8
        file_path, file_size = save_file(data_file.stream, filename, guess['encoding'])

        dataimport = create_base_import(upload_id, file_path, file_size, base_name, base_desc)

        return jsonify(name=filename,
                       size=file_size)


    @route("/peek", methods=["POST"])
    def peek(self):
        # La muestra de los primeros bytes vienen como octet-stream en request.data
        # Por las dudas, cortar la linea trunca (por ajax vienen 1k bytes)

        try:
            fields = detect_fields(request.data)
        except ValueError:
            return jsonify({"error" : "No se encontraron los delimitadores"})

        return jsonify({"fields" : fields, "id" : uuid.uuid4()})


    @route("/map", methods=["POST"])
    def map(self):
        """
        Recibe el mapeo de los campos
        """
        upload_id = request.form.get('uploadId')
        dataimport = DataImport.get(DataImport.upload_id == upload_id)
        csvmap = CSVMap.create(
                dataimport = dataimport.id,

                name=request.form.getlist('name[]'),
                name_last=request.form.getlist('name_last[]'),
                name_name=request.form.getlist('name_name[]'),

                birth_year=request.form.getlist('birth_year[]'),
                birth_date=request.form.getlist('birth_date[]'),
                gender=request.form.getlist('gender[]'),
                
                personal_id=request.form.getlist('personal_id[]'),
                personal_id_type=request.form.getlist('personal_id_type[]'),
                personal_id_number=request.form.getlist('personal_id_number[]'),

                phone=request.form.getlist('phone[]'),
                phone_code=request.form.getlist('phone_code[]'),
                phone_number=request.form.getlist('phone_number[]'),
                
                email=request.form.getlist('email[]'),

                address=request.form.getlist('address[]'),
                address_postalcode=request.form.getlist('address_postalcode[]'),
                address_city=request.form.getlist('address_city[]'),
                address_province=request.form.getlist('address_province[]'),
                address_country=request.form.getlist('address_country[]'),

                mobile=request.form.getlist('mobile[]'),
                mobile_code=request.form.getlist('mobile_code[]'),
                mobile_number=request.form.getlist('mobile_number[]'),

                social_fb_username=request.form.getlist('social_fb_username[]')
        )

        # Arrancar tarea
        tasks.process_import.delay(dataimport.id)

        # Actializar esado
        dataimport.status = "queued"
        dataimport.save()

        return jsonify({})


ImportsView.register(api)

