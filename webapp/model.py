# -*- coding:utf-8 -*-
from peewee import *
from playhouse.postgres_ext import *
import datetime

import webapp

field_labels = [
    ("name",               u"Nombre"),
    ("name_last",          u"Nombre"),
    ("name_name",          u"Nombre"),
    ("personal_id",        u"ID"),
    ("personal_id_type",   u"ID"),
    ("personal_id_number", u"ID"),
    ("phone",              u"Teléfono"),
    ("phone_code",         u"Teléfono"),
    ("phone_number",       u"Teléfono"),
    ("mobile",             u"Celular"),
    ("mobile_code",        u"Celular"),
    ("mobile_number",      u"Celular"),
    ("email",              u"Email"),
    ("address",            u"Dirección"),
    ("address_postalcode", u"Código Postal"),
    ("address_city",       u"Ciudad"),
    ("address_province",   u"Provincia"),
    ("address_country",    u"País"),
    ("gender",             u"Género"),
    ("birth_date",         u"Nacimiento"),
    ("birth_year",         u"Clase"),
    ("social_fb_username", u"Usuario FB")
]

status_labels = [
    ("uploading-file", "Recibiendo archivo"),
    ("waiting-map",    "Esperando mapeo"),
    ("queued",         "En cola"),
    ("processing",     "Procesando"),
    ("error",          "Error"),
    ("finished",       "Agregado"),
]


class PersonFieldsNormalizer():

    def __init__():
        pass

    def set_address(txt_address, city=None, province=None, country=None):
        self.address = txt_address
        self.address_city = city
        self.address_province = province
        self.address_country = country

    def get_address_city():
        return self.address_city

    def get_address_country():
        return self.address_country

    def get_address_province():
        return self.address_province

    def set_birth(txt_birth_date, year=None):
        pass


class BaseModel(Model):
    class Meta:
        database = webapp.database


class DataImport(BaseModel):
    # FIXME uuid(upload_id) viene del cliente?
    upload_id = CharField()
    file_path = CharField()
    name = CharField(null=True)
    desc = TextField(null=True)

    fields = ArrayField(CharField, null=True)

    row_count = IntegerField(null=True)
    col_count = IntegerField(null=True)
    file_size = IntegerField()

    # Comienza en uploading-file, cuando lo sube espera el map y comienza el procesado hasta 'finished'
    status = CharField(choices=['uploading-file', 'waiting-map', 'queued', 'processing', 'error', 'finished'])

    created_on = DateTimeField(default=datetime.datetime.utcnow)

    class Meta:
        indexes = (
                (('upload_id',  ), True),
                (('row_count',  ), False),
                (('status',     ), False),
                (('created_on', ), False),
        )


class CSVMap(BaseModel):
    dataimport = ForeignKeyField(DataImport, on_delete='CASCADE', related_name='map')

    name = ArrayField(CharField)
    name_last = ArrayField(CharField)
    name_name = ArrayField(CharField)

    gender = ArrayField(CharField)
    birth_date = ArrayField(CharField)
    birth_year = ArrayField(CharField)

    personal_id = ArrayField(CharField)
    personal_id_type = ArrayField(CharField)
    personal_id_number = ArrayField(CharField)

    phone = ArrayField(CharField)
    phone_code = ArrayField(CharField)
    phone_number = ArrayField(CharField)

    email = ArrayField(CharField)

    address = ArrayField(CharField)
    address_postalcode = ArrayField(CharField)
    address_city = ArrayField(CharField)
    address_province = ArrayField(CharField)
    address_country = ArrayField(CharField)

    mobile = ArrayField(CharField)
    mobile_code = ArrayField(CharField)
    mobile_number = ArrayField(CharField)

    social_fb_username = ArrayField(CharField)


class Person(BaseModel):
    dataimport = ForeignKeyField(DataImport, on_delete='CASCADE')
    name = CharField(null=True)
    email = CharField(null=True)
    personal_id_number = IntegerField(null=True)

    birth_date = DateField(null=True)
    gender = CharField(choices=[("m", "Hombre"), ("f", "Mujer")], null=True)

    class Meta:
        indexes = (
                (('email',              ), True),
                (('personal_id_number', ), True),
                (('birth_date',         ), False),
        )


class Address(BaseModel):
    person = ForeignKeyField(Person, on_delete='CASCADE')
    address = CharField()
    country = CharField(null=True)
    province = CharField(null=True)
    city = CharField(null=True)


class Phone(BaseModel):
    person = ForeignKeyField(Person, on_delete='CASCADE')
    area_code = CharField(null=True)
    number = CharField(null=True)


class Mobile(BaseModel):
    person = ForeignKeyField(Person, on_delete='CASCADE')
    area_code = CharField(null=True)
    number = CharField(null=True)


class SocialProfile(BaseModel):
    person = ForeignKeyField(Person, on_delete='CASCADE')
    entity = CharField(choices=["Facebook", "Twitter", "Foursquare"])
    username = CharField()
    profile_url = CharField(null=True)


class Location(BaseModel):
    """
    Para almacenar geo devuelto por campañas de Email
    """
    pass


webapp.database.create_tables([Person, DataImport, CSVMap, Address, Mobile, Phone, SocialProfile], safe=True)

"""
from playhouse.migrate import *
migrator = PostgresqlMigrator(webapp.database)
migrate(
       migrator.add_column('csvmap', 'address_postalcode', CSVMap.address_postalcode)
)

"""
