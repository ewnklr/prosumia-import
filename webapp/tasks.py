# -*- coding:utf-8 -*-
"""
"""
import celery
from celery.utils.log import get_task_logger
import csv

from webapp.model import DataImport, CSVMap, Person
from webapp import database
from webapp import helpers


# Celery y su configuración en celeryconfig.py
app = celery.Celery('abm')
app.config_from_object('webapp.celeryconfig')

# Para logear
logger = get_task_logger(__name__)


@app.task
def process_import(dataimport_id):
    """
    Procesa los campos de un archivo CSV
    """
    try:
        dataimport = DataImport.get(DataImport.id == dataimport_id)
    except Exception:
        logger.warning("Could not find the import. Ignoring...")

    csvfile = open(dataimport.file_path, "rb")

    # Guess de delimitadores y campos de un sample
    sample = csvfile.read(4 * 2**10)
    csvfile.seek(0)
    try:
        dialect = csv.Sniffer().sniff(sample)
        has_header = csv.Sniffer().has_header(sample)
    except csv.Error:
        logger.critical("No se pueden obtener dialecto del archivo '%s'", dataimport.file_path)
        # escribir status con error

    if not has_header:
        logger.critical("El archivo '%s' no posee la cabecera", dataimport.file_path)
        # escribir status con error

    csvmap = CSVMap.get(dataimport == dataimport.id)

    # Ya con el dialecto, cabecera y mapping, leo el archivo que ya guardé en utf-8
    for row in helpers.UnicodeReader(csvfile, dialect=dialect, encoding='utf-8'):

        sp = PersonFieldsNormalizer()

        sp.set_birth(date=row.get(csvmap.birth_date), year=row.get(csvmap.birth_year))
        sp.set_name(row.get(csvmap.name), name=row.get(csvmap.name_name), last_name=row.get(csvmap.name_last))
        sp.set_gender(row.get(csvmap.gender))
        sp.set_email(row.get(csvmap.email))
        sp.set_phone(row.get(csvmap.phone) or row.get(csvmap.phone_number), code=row.get(csvmap.phone_code))
        sp.set_mobile(row.get(csvmap.mobile) or row.get(csvmap.mobile_number), code=row.get(csvmap.mobile_number))
        sp.set_personal_id(row.get(csvmap.personal_id) or row.get(csvmap.personal_id_number), type=row.get(csvmap.personal_id_type))
        sp.set_address(row.get(csvmap.address), city=row.get(csvmap.city), province=row.get(csvmap.province), country=row.get(csvmap.country))

        # Ahora intento encontrar la persona por DNI o EMAIL para agregarle los datos
        person = Person.create_or_get(email=sp.get_email(), personal_id_number=sp.get_personal_id_number())
        person.birth = sp.get_birth()
        person.birth_year = sp.get_birth_year()
        person.gender = sp.get_gender()
        person.save()

        person_address = Address.reate_or_get(person=person)
        person_address.address = sp.get_address()
        person_address.city = sp.get_address_city()
        person_address.province = sp.get_address_province()
        person_address.country = sp.get_address_country()
        person_address.save()

        person_phone = Phone.create_or_get(number=sp.get_phone_number(), area_code=sp.sp.get_phone_code(), person=person)
        person_mobile = Mobile.create_or_get(number=sp.get_mobile_number(), area_code=sp.sp.get_mobile_code(), person=person)


        


