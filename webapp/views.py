from flask import render_template

from webapp import abm
from webapp.helpers import requires_auth


@abm.route("/")
@requires_auth
def overview():
    return render_template("overview.html")


@abm.route("/import")
@requires_auth
def import_database():
    return render_template("import.html")

@abm.route("/resume-import/<upload_id>")
@requires_auth
def resume_import_database(upload_id):
    return render_template("import.html", upload_id=upload_id)
