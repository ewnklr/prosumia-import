# -*- coding:utf-8 -*-
import uuid
import requests
from flask.ext.script import Manager
from flask import jsonify, current_app, url_for
import cchardet
from werkzeug import secure_filename
from StringIO import StringIO

from webapp import application
from webapp.model import DataImport
from webapp.api.views import save_file, create_base_import
from webapp.model import DataImport, CSVMap, field_labels, status_labels

manager = Manager(application)


def walk_unique_persons():
    query = DataImport.select(DataImport, CSVMap).join(CSVMap).order_by(DataImport.id.desc())

    for dataimport in query:

        results.append({
            "name":    dataimport.name,
            "desc":    dataimport.desc,
            "count":   dataimport.row_count,
            "fields":  ", ".join(mapped_fields(dataimport.map)),
            "status":  status_label_dict[dataimport.status],
            "actions": render_template("_actions.html")
        })


def __get_file_with_fields(field):
    pass


def walk_complete_phone_numbers():
    """
    Generador con todos los números de telefono en los csv
    """
    pass


@manager.command
def upload_from_url(url):
    upload_id = str(uuid.uuid4())
    base_desc = ""
    base_name = ""

    data_file, filename = download_file(url)

    filename = secure_filename(upload_id + "_" + filename)

    # Muestra del archivo para detectar encoding
    sample = data_file.read(10 * 2**20)
    data_file.seek(0)
    guess = cchardet.detect(sample)

    # Se guarda en utf-8
    file_path, file_size = save_file(data_file, filename, guess['encoding'])

    dataimport = create_base_import(upload_id, file_path, file_size, base_name, base_desc)

    print filename
    print url_for("abm.resume_import_database", upload_id=upload_id)


@manager.command
def purge_uploads():
    q = DataImport.delete().where(DataImport.name=='asd')
    q.execute()


def download_file(url):
    filename = url.split('/')[-1]
    r = requests.get(url, stream=True)

    stream = StringIO()
    for chunk in r.iter_content(chunk_size=1024): 
        if chunk:
            stream.write(chunk)

    stream.seek(0)
    return stream, filename


manager.run()

